# set XDG DIRS
XDG_CONFIG_HOME="$HOME/.config" export XDG_CONFIG_HOME
XDG_CACHE_HOME="$HOME/.cache" export XDG_CACHE_HOME
XDG_DATA_HOME="$HOME/.local/share" export XDG_DATA_HOME

# set path
PATH="$HOME/.local/bin:$PATH" export PATH

# load mkshrc
ENV=$HOME/.config/mksh/mkshrc export ENV

# disable touchpad
xinput set-prop "ETPS/2 Elantech Touchpad" 163 0 

# Plan9
PLAN9="$HOME/apps/plan9" export PLAN9
PATH="$PATH:$PLAN9/bin" export PATH

# set locale
LANG=en_US.UTF-8 export LANG

# autologin on tty1
if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
   exec startx
fi
